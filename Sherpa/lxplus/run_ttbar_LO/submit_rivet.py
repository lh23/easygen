#!/usr/bin/env python3

import glob

clusterid=None
with open('CLUSTERID.txt') as f:
    lines = f.readlines()
    clusterid=lines[0].strip()

inbasepath="/eos/user/m/mcfayden/www/public/LH23/easygen/Sherpa/lxplus/run_ttbar_LO/"
inprefix="root://eosuser.cern.ch/"
infilelist=[]
print(f"{inbasepath}/events.hepmc.{clusterid}.*.tgz")
for infile in glob.glob(f"{inbasepath}/events.hepmc.{clusterid}.*.tgz"):
    print(infile)
    infilelist.append(inprefix+infile)
#events.hepmc.5598135.1.tgz,root://eosuser.cern.ch//eos/user/m/mcfayden/www/public/LH23/easygen/Sherpa/lxplus/run_ttbar_LO/events.hepmc.5598135.2.tgz

infiles=f",".join(infilelist)


subcmd=f"""
executable              = rivet.sh
arguments               = {clusterid}
output                  = output/SherpaRivet.$(ClusterId).out
error                   = error/SherpaRivet.$(ClusterId).err
log                     = log/SherpaRivet.$(ClusterId).log

+SingularityImage     = "/cvmfs/unpacked.cern.ch/registry.hub.docker.com/hepstore/rivet-sherpa:3.1.6"

should_transfer_files   = YES
transfer_input_files    = {infiles}
output_destination 	= {inprefix}{inbasepath}
transfer_output_files 	= Sherpa.yoda
MY.XRDCP_CREATE_DIR     = True

queue
"""
print(subcmd)
with open(f'rivet.{clusterid}.sub','w') as sf:
    sf.write(subcmd)

import subprocess
process = subprocess.Popen(["condor_submit", f'rivet.{clusterid}.sub'], stdout=subprocess.PIPE)
out, err = process.communicate()
#print(out,err)
clusterid=str(out.decode('utf-8').split()[-1][:-1])
print(f"Submitted {clusterid}!")

##Doesnt work due to some issue with eos creds?
#import os
#import htcondor
#print(htcondor.version())
#schedd = htcondor.Schedd()
#
##col = htcondor.Collector()
##credd = htcondor.Credd()
##credd.add_user_cred(htcondor.CredTypes.Kerberos, None)
#
#sub = htcondor.Submit(subcmd)
#with schedd.transaction() as txn:
#    clusterid=sub.queue(txn)
#    
#print(clusterid)
#
#sub = htcondor.Submit()
#sub['Executable'] = "/afs/cern.ch/user/b/bejones/tmp/condor/hello.sh"
#sub['Error'] = "/afs/cern.ch/user/b/bejones/tmp/condor/error/hello-$(ClusterId).$(ProcId).err"
#sub['Output'] = "/afs/cern.ch/user/b/bejones/tmp/condor/output/hello-$(ClusterId).$(ProcId).out"
#sub['Log'] = "/afs/cern.ch/user/b/bejones/tmp/condor/log/hello-$(ClusterId).log"
#sub['MY.SendCredential'] = True
#sub['+JobFlavour'] = '"tomorrow"'
#sub['request_cpus'] = '1'
#
#schedd = htcondor.Schedd()
#>>> with schedd.transaction() as txn:
#...     cluster_id = sub.queue(txn)
#...
#>>> print(cluster_id)
