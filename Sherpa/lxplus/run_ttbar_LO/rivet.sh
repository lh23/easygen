#!/bin/bash

echo "Starting Sherpa Generation"


gen=Sherpa
res=lxplus
proc=run_ttbar_LO
anas=MC_TTBAR
jobid=$1

run=$gen/$res/$proc

basepath=/afs/cern.ch/work/m/mcfayden/LH23/easygen/$run
outpath=/eos/user/m/mcfayden/www/public/LH23/easygen/$run

ls -larth

echo "INFO   : =========================="
echo "INFO   : =========================="
echo "INFO   : Run settings:"
echo "INFO   : Generator = $gen"
echo "INFO   : Resource  = $res"
echo "INFO   : Process   = $proc"
echo "INFO   : JobId     = $jobid"

echo "INFO   : =========================="
#echo "INFO   : Untarring input files:"
echo "INFO   : Input files:"
#ls events.hepmc.$jobid.*.tgz
for i in events.hepmc.$jobid.*.tgz; do
    echo "INFO   : - $i"
    tar xvzf $i
done

echo "INFO   : =========================="
echo "INFO   : Starting Rivet run"
echo "INFO   : - version:  "$(rivet --version)
echo "INFO   : - analyses: "$anas

rivet --version
#rivet -a $anas events.hepmc.$jobid.*.tgz -H Sherpa.yoda
rivet -a $anas events.$jobid.*.hepmc -H Sherpa.yoda

echo "DEBUG  : ls -larth"
ls -larth


rivet-mkhtml --mc-errs Sherpa.yoda
#ls -larth
#mv events.hepmc.tgz rivet-plots/
#mv Rivet.yoda rivet-plots/
#export EOS_MGM_URL=root://eosuser.cern.ch
#cp -r rivet-plots $outpath/
#mkdir rivet-plots
#echo "hellow world etst2" > rivet-plots/hello.txt
#echo "hello world2 crap" > hello2.txt
#echo "Trying to copy output..."
#cp -r rivet-plots $outpath/
rsync -r rivet-plots $outpath/
touch Sherpa.yoda

echo "End of job"
