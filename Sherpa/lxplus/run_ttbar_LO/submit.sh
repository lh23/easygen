# Make output dirs and clean up old runs
mkdir -p output error log
mkdir -p /eos/user/m/mcfayden/www/public/LH23/CRAP/GenPlusRivet/Sherpa/run_ttbar_LO/
if [ -d /eos/user/m/mcfayden/www/public/LH23/CRAP/GenPlusRivet/Sherpa/run_ttbar_LO/rivet-plots ]; then
    rm -fr /eos/user/m/mcfayden/www/public/LH23/CRAP/GenPlusRivet/Sherpa/run_ttbar_LO/rivet-plots;
fi

# Send job
condor_submit run.sub


#@TODO
# - Submit multiple subjobs for different random seeds
# - Merge yoda files before plotting
