#!/bin/bash

echo "Starting Sherpa Generation"


gen=Sherpa
res=lxplus
proc=run_ttbar_LO
anas=MC_TTBAR
nevt=10000
jobid=$1
seed=$2

run=$gen/$res/$proc

basepath=/afs/cern.ch/work/m/mcfayden/LH23/easygen/$run
outpath=/eos/user/m/mcfayden/www/public/LH23/easygen/$run

echo "INFO   : =========================="
echo "INFO   : =========================="
echo "INFO   : Run settings:"
echo "INFO   : Generator = $gen"
echo "INFO   : Resource  = $res"
echo "INFO   : Process   = $proc"
echo "INFO   : =========================="
echo "INFO   : "
#echo "WARNING: "
#echo "ERROR  : "

echo "INFO   : Getting run card"
cp $basepath/Run.dat .
echo "INFO   : Print run card"
cat Run.dat
echo "INFO   : =========================="
echo "INFO   : Starting $gen run"
echo "INFO   : - version: "$($gen --version)
echo "INFO   : - nevents: "$nevt
echo "INFO   : - seeds: "$seed
Sherpa -e$nevt -R$seed EVENT_OUTPUT=HepMC3_GenEvent[events.$jobid.$seed.hepmc]

echo "INFO   : Making tarball of HepMC file"
tar cvzf events.hepmc.$jobid.$seed.tgz events.$jobid.$seed.hepmc

echo "DEBUG  : ls -larth"
ls -larth

echo "INFO   : =========================="
echo "INFO   : Starting Rivet run"
echo "INFO   : - version:  "$(rivet --version)
echo "INFO   : - analyses: "$anas

rivet --version
rivet -a $anas events.$jobid.$seed.hepmc

echo "DEBUG  : ls -larth"
ls -larth


#rivet-mkhtml --mc-errs Rivet.yoda
#ls -larth
#mv events.hepmc.tgz rivet-plots/
#mv Rivet.yoda rivet-plots/
#export EOS_MGM_URL=root://eosuser.cern.ch
#cp -r rivet-plots $outpath/
#mkdir rivet-plots
#echo "hellow world etst2" > rivet-plots/hello.txt
#echo "hello world2 crap" > hello2.txt
#echo "Trying to copy output..."
#cp -r rivet-plots $outpath/
#rsync -r rivet-plots $outpath/
echo "End of job"
