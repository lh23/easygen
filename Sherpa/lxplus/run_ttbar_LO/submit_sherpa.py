#!/usr/bin/env python3

import glob

#import os

#mkdir -p output error log
#mkdir -p /eos/user/m/mcfayden/www/public/LH23/easygen/Sherpa/lxplus/run_ttbar_LO/
#if [ -d /eos/user/m/mcfayden/www/public/LH23/easygen/Sherpa/lxplus/run_ttbar_LO/rivet_plots ]; then
#    rm -fr /eos/user/m/mcfayden/www/public/LH23/easygen/Sherpa/lxplus/run_ttbar_LO/rivet-plots;
#fi

# Send job
#condor_submit run.sub | tee sub.txt
#cat sub.txt | grep "submitted to cluster" | awk '{print $NF;}' | sed 's/\.//g' > CLUSTERID.txt


njobs=2

outbasepath="/eos/user/m/mcfayden/www/public/LH23/easygen/Sherpa/lxplus/run_ttbar_LO/"
outprefix="root://eosuser.cern.ch/"

subcmd=f"""
seed			= $$([ ProcId + 1 ])
executable              = run.sh
arguments               = $(ClusterId) $(seed)
output                  = output/SherpaRun.$(ClusterId).$(seed).out
error                   = error/SherpaRun.$(ClusterId).$(seed).err
log                     = log/SherpaRun.$(ClusterId).log

+SingularityImage     = "/cvmfs/unpacked.cern.ch/registry.hub.docker.com/hepstore/rivet-sherpa:3.1.6"
should_transfer_files   = YES

output_destination 	= {outprefix}{outbasepath}
transfer_output_files 	= events.hepmc.$(ClusterId).$(seed).tgz
MY.XRDCP_CREATE_DIR     = True

queue {njobs}
"""

print(subcmd)
with open(f'sherpa.sub','w') as sf:
    sf.write(subcmd)

import subprocess
process = subprocess.Popen(["condor_submit", f'sherpa.sub'], stdout=subprocess.PIPE)
out, err = process.communicate()
#print(out,err)
clusterid=str(out.decode('utf-8').split()[-1][:-1])
print(f"Submitted {clusterid}!")

with open('CLUSTERID.txt','w') as cf:
    cf.write(clusterid)


##Doesnt work due to some issue with eos creds?
#import os
#import htcondor
#print(htcondor.version())
#schedd = htcondor.Schedd()
#
##col = htcondor.Collector()
##credd = htcondor.Credd()
##credd.add_user_cred(htcondor.CredTypes.Kerberos, None)
#
#sub = htcondor.Submit(subcmd)
#with schedd.transaction() as txn:
#    clusterid=sub.queue(txn)
#    
#print(clusterid)
#
#sub = htcondor.Submit()
#sub['Executable'] = "/afs/cern.ch/user/b/bejones/tmp/condor/hello.sh"
#sub['Error'] = "/afs/cern.ch/user/b/bejones/tmp/condor/error/hello-$(ClusterId).$(ProcId).err"
#sub['Output'] = "/afs/cern.ch/user/b/bejones/tmp/condor/output/hello-$(ClusterId).$(ProcId).out"
#sub['Log'] = "/afs/cern.ch/user/b/bejones/tmp/condor/log/hello-$(ClusterId).log"
#sub['MY.SendCredential'] = True
#sub['+JobFlavour'] = '"tomorrow"'
#sub['request_cpus'] = '1'
#
#schedd = htcondor.Schedd()
#>>> with schedd.transaction() as txn:
#...     cluster_id = sub.queue(txn)
#...
#>>> print(cluster_id)
